<h1>CustomerCard</h1>
<form action="/edit" method="POST">
    @csrf
    <input type="hidden" name="id" value="{{$data['id']}}">
    <input type="text" name="customer_name_surname" value="{{$data['customer_name_surname']}}"><br><br>
    <input type="text" name="phone_number" value="{{$data['phone_number']}}"><br><br>
    <input type="text" name="email" value="{{$data['email']}}"><br><br>
    <input type="text" name="address" value="{{$data['address']}}"><br><br>
    <button type="submit">Uptade</button>
</form>
@if (session('alert'))
    <div class="alert alert-success">
        {{ session('alert') }}
    </div>
@endif