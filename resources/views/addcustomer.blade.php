@extends('layouts.app')

@section('content')
@auth
<h1>Add Customers</h1>
<form action="add" method="POST">
    @csrf
    <input type="text" name="customer_name_surname" placeholder="Enter Name"> <br><br>
    <input type="text" name="phone_number" placeholder="Enter Phone number"> <br><br>
    <input type="text" name="email" placeholder="Enter email"> <br><br>
    <input type="text" name="address" placeholder="address"> <br><br>
    <button type="submit" >Add Customer</button>
    

</form>
@endauth
@endsection
@if (session('alert'))
    <div class="alert alert-success">
        {{ session('alert') }}
    </div>
@endif
