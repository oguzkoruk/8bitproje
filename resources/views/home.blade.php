@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Welcome') }}</div>
                
                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    
                    {{ __('You are logged !') }}
                  
                </div>
                <a href="http://127.0.0.1:8000/list" class="text-sm text-gray-900 underline">Customerlist</a>
                <a href="http://127.0.0.1:8000/add" class="text-sm text-gray-900 underline">AddCustomer</a>
            </div>
        </div>
    </div>
</div>
@endsection
