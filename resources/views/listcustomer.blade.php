@extends('layouts.app')

@section('content')
@auth
<h1> Customer List </h1>
<table border="1">
    <tr>
        <td>Id</td>
        <td>Customer_name_surname</td>
        <td>Phone_number</td>
        <td>Email</td>
        <td>Address</td>
        <td>Operations</td>
    </tr>
    @foreach($customers as $customer)
    <tr>
        <td>{{$customer['id']}}</td>
        <td>{{$customer['customer_name_surname']}}</td>
        <td>{{$customer['phone_number']}}</td>
        <td>{{$customer['email']}}</td>
        <td>{{$customer['address']}}</td>
        <td><a href={{"delete/" .$customer['id']}}>Delete</a></td>
        <td><a href={{"edit/" .$customer['id']}}>Customercard</a></td>
    </tr>
    @endforeach
</table>
<span>
{{$customers->links()}}
</span>

@endauth
@endsection
<style>
    .w-5{
        display: none
    }
</style>
@if (session('alert'))
    <div class="alert alert-success">
        {{ session('alert') }}
    </div>
@endif
