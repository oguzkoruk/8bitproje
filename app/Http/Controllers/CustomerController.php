<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Customer;
use GuzzleHttp\Psr7\Request as Psr7Request;
use Illuminate\Mail\Events\MessageSent;
use Symfony\Component\HttpFoundation\Request as HttpFoundationRequest;


class CustomerController extends Controller
{
 
    function getData()
    {
        $data=Customer::paginate(10);
        return view('listcustomer',['customers'=>$data]);
       // return view('listcustomer');
    }
    function addcustomer(Request $req) 
    {
        $customer=new Customer;
        $customer->customer_name_surname=$req->customer_name_surname;
        $customer->phone_number=$req->phone_number;
        $customer->email=$req->email;
        $customer->address=$req->address;
        $customer->save();
        return redirect('add')->with('alert', 'Customer Added!');;
        
    }
    function delete($id)
    {
        $data=Customer::find($id);
        $data->delete();
        return redirect('list');
    }
    function getCustomercard($id)
    {
       $data= Customer::find($id);
       return view('edit',['data'=>$data]);
    }
    function uptade(Request $req)
    {
        $data= Customer::find($req->id);
        $data->customer_name_surname=$req->customer_name_surname;
        $data->phone_number=$req->phone_number;
        $data->email=$req->email;
        $data->address=$req->address;
        $data->save();
        
        
          
        return  redirect('list')->with('alert', 'Uptaded!');
        

    }
}
