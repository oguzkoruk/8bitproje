<?php

use Illuminate\Support\Facades\Route;
use App\Models\Customer;
Use App\Http\Controllers\CustomerController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();
Route::view('add','addcustomer');
Route::post('add',[CustomerController::class,'addcustomer']);
Route::get('list',[CustomerController::class,'getData']);
Route::get('delete/{id}',[CustomerController::class,'delete']);
Route::get('edit/{id}',[CustomerController::class,'getCustomercard']);
Route::post('edit',[CustomerController::class,'uptade']);

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
